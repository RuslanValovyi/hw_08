Your homework is to create a custom string class. But don't cheat and make it a wrapper for std::string :-)

You must implement next methods and operators, the rest is up to you if you want to do more -- please do:
+ copy constructor
+ move constructor
+ destructor
+ length
reverse() // to return a reversed copy
insert // insert a substring or character into specified position
append // to append a substring/character
remove // to remove a substring/character from specified position
contains // to check if it contains specified substring. if so, return its position
toStdString // to convert into a std::string

+ operators ==, =, +, «, », std::string


(it is desirable that your class could accept std::string as well as itself)
Please also add working examples of usage of these methods.

bonus:
add split method, which splits original string using given delimiter into a substrings and  returns a list of these substrings
