#include <cstring>
#include "my_string.h"

String::String()
    : sz(0), s{new char[s_size]} {}

// Copy-constructor (const char [])
String::String(const char in[])
    try {
        sz = static_cast<int>(std::strlen(in));
        s = new char[s_size];
        std::copy(in, in + sz, s);
    } catch (...) {
        if (s) {
            delete[] s;
        }
    }

// Copy-constructor (std::string)
String::String(std::string in)
    try {
        sz = static_cast<int>(in.length());
        s = new char[s_size];
        in.copy(s, sz);
    } catch (...) {
        if (s) {
            delete[] s;
        }
    }

// Copy-constructor (const String)
String::String(const String& in)
    try {
        sz = in.sz;
        s = new char[s_size];
        strcpy(s, in.s);
    } catch (...) {
        if (s) {
            delete[] s;
        }
    }

// Move constructor
String::String(String&& in)
    try 
    : sz(in.sz)
    , s(in.s) {
        in.sz = 0;
        in.s = nullptr;
    } catch (...) {
        if (s) {
            delete[] s;
        }
    }

// Destructor
String::~String()
    try {
        delete[] s;
    }
    catch (...) {
        if (s) {
            delete[] s;
        }
    }

String::operator std::string() {
    return "It's a String class"; 
}
        
void String::operator=(const String &string){
    sz = string.sz;
    std::copy(string.s, string.s + sz, s);
    s[sz]='\0';
}

void String::operator+(const String &string){
    sz += string.sz;
    strcat(s, string.s);
}

bool String::operator==(const String &string){
    bool result;
    if( std::strcmp(s,string.s)){
        result = false;
    }
    else {
        result = true;
    }
    return result;
}

void String::operator=(const std::string &string){
    sz = static_cast<int>(string.length());
    string.copy(s, sz);
}

void String::operator+(const std::string &string){
    sz += string.length();
    strcat(s, string.c_str());
}

bool String::operator==(const std::string &string){
    bool result;
    if( std::strcmp(s,string.c_str())){
        result = false;
    }
    else
        result = true;
    return result;
}

std::ostream& operator<<(std::ostream& out, const String& string) {
    return out << string.s;
}

std::istream& operator>> (std::istream &in, String &string)
{
    in >> string.s;
    string.sz = static_cast<int>(std::strlen(string.s));
 
    return in;
}

char* String::reverse() 
{ 
    char* out = new char[sz + 1];
    int n=sz-1;
    for(int i=0; i<sz; i++){
      out[i] = s[n];
      n = n-1;
    }
    out[sz] = '\0';
    return out;
} 

char* String::insert(const char in[], int offset) 
{ 
    int szi = static_cast<int>(std::strlen(in));
    char* out = new char[sz + szi + 1];
    std::copy(s, s + offset, out);
    std::copy(in, in + szi, out + offset);
    std::copy(s + offset, s + sz, out + szi + offset);
    out[sz + szi] = '\0';
    return out;
} 

char* String::append(const char in[]) 
{ 
    int szi = static_cast<int>(std::strlen(in));
    char* out = new char[sz + szi + 1];
    std::copy(s, s + sz, out);
    std::copy(in, in + szi, out + sz);
    out[sz + szi] = '\0';
    return out;
} 

char* String::remove(int offset) 
{ 
    char* out = new char[sz - offset + 1];
    std::copy(s, s + offset, out);
    out[offset] = '\0';
    return out;
} 

bool String::contains(const char in[]) 
{ 
    bool result = false;
    int szi = static_cast<int>(std::strlen(in));
    for(int i=0; i<sz; i++){
        if (!std::strncmp(in, s + i, szi)){
            result = true;
            break;
        }
    }
    return result;
} 

std::string String::toStdString()
{
    return static_cast<std::string>(s);
}

std::list<const char *>* String::split()
{
    auto result = new std::list<const char *>;
    char * token = strtok(s, " -.");
    while( token != nullptr ) {
        result->push_back(token);
        token = strtok(nullptr, " -.");
    }
    return result;
}














