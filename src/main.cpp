#include "my_string.h"
#include <iostream>

int main() {
    String s1("Clang compiler");
    std::cout << "Copy constructor(const char []): " << s1 << std::endl;
    std::cout << "s1 size: " << s1.length() << std::endl << std::endl;
    
    std::string s2("Object std::string");
    String obj1(s2);
    std::cout << "Copy constructor(std::string) " << obj1 << std::endl << std::endl;
    
    String obj2(s1);
    std::cout << "Copy constructor(String) " << obj2 << std::endl << std::endl;
    
    auto tmp = String("Move constructor test");
    std::cout << "tmp size is " << tmp.length() << std::endl;
    String s4(std::move(tmp));    
    std::cout << "tmp size is " << tmp.length() << std::endl;
    std::cout << "Move constructor(String): " << s4 << std::endl << std::endl;
    
    std::cout << "Operator 'std::string': " << std::string(s1) << std::endl << std::endl;
    
    String s1_1 = s1;
    std::cout << "Operator '=': " << s1_1 << std::endl << std::endl;
    
    String t1("Trampoline ");
    String t2("is working");
    
    if (t1 == t2) {
        std::cout << "Operator '==' doesn't work." << std::endl << std::endl;
    } 
    else {
        std::cout << "Operator '==' works well." << std::endl << std::endl;
    }
    
    t1 + t2;
    std::cout << "Operator '+': " << t1 << std::endl << std::endl;
    
    auto r = s1.reverse();
    std::cout << "reverse(): " << r << std::endl << std::endl;
    delete [] r;
    
    auto r2 = s1.insert("- ", 6);
    std::cout << "insert(): " << r2 << std::endl << std::endl;
    delete [] r2;
    
    auto r3 = s1.append(" - good compiler");
    std::cout << "append(): " << r3 << std::endl << std::endl;
    delete [] r3;
    
    auto r4 = s1.remove(5);
    std::cout << "remove(): " << r4 << std::endl << std::endl;
    delete [] r4;
    
    if (s1.contains("comp")) {
        std::cout << "'comp' is present in s1." << std::endl << std::endl;
    } 
    else {
        std::cout << "'comp' isn't present in s1." << std::endl << std::endl;
    }
    
    std::cout << "'toStdString': " << s1.toStdString() << std::endl << std::endl;
    
    String sp("When half way through the journey of our life - I found that I was in a gloomy wood.");
    auto l = sp.split();
    for (auto iter = l->begin(); iter != l->end(); iter++) {
        std::cout << *iter << std::endl;
    }
    std::cout << std::endl;
    delete l;
    
    String s3;
    std::cout << "Enter some string: ";
    std::cin >> s3;
    std::cout << "String s1 is '" << s3 << "'" << std::endl << std::endl;
   
}
