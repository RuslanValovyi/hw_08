#pragma once
#include <iostream>
#include <list>

class String {
  public:
    String();
	String(const char in[]);
    String(std::string in);
    String(const String& in);
    String(String&& in);
	~String();
    
    friend std::ostream& operator<<(std::ostream& out, const String& string);
    friend std::istream& operator>>(std::istream &in, String &string);
    operator std::string();   
    void operator= (const String &string);
    void operator+ (const String &string);
    bool operator== (const String &string);
    void operator=(const std::string &string);
    void operator+(const std::string &string);
    bool operator==(const std::string &string);
 
    char* reverse(); // to return a reversed copy
    char* insert(const char in[], int offset); // insert a substring or character into specified position
    char* append(const char in[]); // to append a substring/character
    char* remove(int offset); // to remove a substring/character from specified position
    bool contains(const char in[]); // to check if it contains specified substring. if so, return its position
    std::string toStdString(); // to convert into a std::string
    std::list<const char *>* split();
    
    int length() const {
        return sz;
    }
  private:
    int sz;
    const int s_size = 255;
    char* s;
};
        
